/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.shapeinheritance;

/**
 *
 * @author domem
 */
public class Square extends Rectangle{
    public Square (double side){
        super(side, side);
    }
    @Override
    public void calArea(){
        System.out.printf("Square: side: "+width+", area = "+(height*width));
        System.out.println();
    }
}
