/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.shapeinheritance;

/**
 *
 * @author domem
 */
public class Circle extends Shape {
    private double r;
    private double pi = 22.0/7;
    
    public Circle(double r){
        this.r = r;
        
    }
    
    @Override
    public void calArea(){
        System.out.printf("Circle: r: "+r+", area = " + (r*r)*pi);
        System.out.println();
    }
}
