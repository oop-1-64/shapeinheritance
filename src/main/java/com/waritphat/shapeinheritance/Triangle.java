/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.shapeinheritance;

/**
 *
 * @author domem
 */
public class Triangle extends Shape{
    private double base;
    private double height;
    
    public Triangle(double base, double height){
        this.base = base;
        this.height = height;
    } 
    @Override
    public void calArea(){
        System.out.printf("Triangle: base: "+base+", height"+ height +", area = "+((height*base)/2));
        System.out.println();
    }
}
