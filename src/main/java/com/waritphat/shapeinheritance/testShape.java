/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.shapeinheritance;

/**
 *
 * @author domem
 */
public class testShape {

    public static void main(String[] args) {
        Circle circle1 = new Circle(4);
//        circle1.calArea();
        Triangle triangle1 = new Triangle(4, 3);
//        triangle1.calArea();
        Rectangle rectangle1 = new Rectangle(4, 3);
//        rectangle1.calArea();
        Square square1 = new Square(2);
//        square1.calArea();
        
        Shape[] shape1 = {circle1, triangle1, rectangle1, square1};
        for (int i = 0; i<shape1.length; i++)
        {
            shape1[i].calArea();
        }

    }
}
