/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.shapeinheritance;

/**
 *
 * @author domem
 */
public class Rectangle extends Shape{
    protected double width;
    protected double height;
    
    public Rectangle(double width, double height){
        this.width = width;
        this.height = height;
    }
    @Override
    public void calArea(){
        System.out.printf("Rectangle: width: "+width+", height"+ height +", area = "+(height*width));
        System.out.println();
    }
}
